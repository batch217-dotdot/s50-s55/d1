import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
export default function CourseCard({courseProp}) {


	const { _id, name, description, price, slots } = courseProp;


    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Slots:
                </Card.Subtitle>
                <Card.Text>
                    {slots} available
                </Card.Text>
                <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
            </Card.Body>
        </Card>
    )
}